<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBigbluemeetings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bigbluemeetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('meetingid');
            $table->string('presen_name');
            $table->string('meetingname');
            $table->integer('instructor_id')->unsigned();
            $table->longtext('detail')->nullable();
            $table->string('start_time');
            $table->string('modpw');
            $table->string('attendeepw');
            $table->string('welcomemsg')->nullable();
            $table->string('duration');
            $table->string('setMaxParticipants')->default(-1);
            $table->string('record')->default('false');
            $table->string('setMuteOnStart')->default('false');
            $table->boolean('allow_record');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bigbluemeetings');
    }
}
